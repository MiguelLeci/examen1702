<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Post"%>
<c:set var="title" value="post List"/>
<c:set var="current" value="post"/>
<%@include file="/WEB-INF/view/header.jsp" %>
<table>
    <thead>
        <tr>
            <th>id</th>
            <th>autor</th>
            <th>titulo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        
    <jsp:useBean id="posts" type="ArrayList" scope="request" />
    <c:forEach items="${posts}" var="post">
        <tr>
            <td>${post.getId()}</td>
            <td>${post.getAuthor()}</td>
            <td>${post.getTitle()}</td>
            <td>
                <div >
                    <a href="/examen1702/post/view/${post.getId()}">Ver</a>
                </div>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
    <div>
        
        <a href="/examen1702/post/index/1">1</a>
        <a href="/examen1702/post/index/2">2</a>
        <a href="/examen1702/post/index/3">3</a>
        <a href="/examen1702/post/index/4">4</a>
        <a href="/examen1702/post/index/5">5</a>
        <a href="/examen1702/post/index/6">6</a>
        <a href="/examen1702/post/index/7">7</a>
        <a href="/examen1702/post/index/8">8</a>
        <a href="/examen1702/post/index/9">9</a>
        <a href="/examen1702/post/index/10">10</a>
        
        
    </div>
<%@include file="/WEB-INF/view/footer.jsp" %>
