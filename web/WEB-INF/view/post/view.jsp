

<%@page import="model.Post"%>
<%@include file="/WEB-INF/view/header.jsp" %>
<jsp:useBean id="post" type="model.Post" scope="request" />
<div id="content">
    <p><%= post.getId()%></p>
    <p><%= post.getAuthor()%></p>
    <p><%= post.getTitle()%></p>
    <p><%= post.getContent()%></p>    
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>