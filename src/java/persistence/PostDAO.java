/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

//import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Post;

/**
 *
 * @author alumno
 */
public class PostDAO extends BaseDAO {

    
    private static final Logger LOG = Logger.getLogger(PostDAO.class.getName());
    
    public PostDAO(){
        super();
    }


    public ArrayList<Post> getAll() {
        PreparedStatement stmt = null;
        ArrayList<Post> posts = null;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from posts");
            ResultSet rs = stmt.executeQuery();
            posts = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Post post = new Post();
                post.setId(rs.getLong("id"));
                post.setAuthor(rs.getString("author"));
                post.setTitle(rs.getString("title"));
                post.setContent(rs.getString("content"));

                posts.add(post);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }
    
    public ArrayList<Post> getPage(int page, int elementsPerPage) {
        ArrayList<Post> posts = new ArrayList<>();
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT * FROM posts" 
                            + " LIMIT ?"
                            + " OFFSET ?"
            );
            stmt.setLong(1, elementsPerPage);
            stmt.setLong(2, offset);
            ResultSet rs = stmt.executeQuery();
            posts = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Post post = new Post();
                post.setId(rs.getLong("id"));
                post.setAuthor(rs.getString("author"));
                post.setTitle(rs.getString("title"));
                post.setContent(rs.getString("content"));

                posts.add(post);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return posts;
    }

    
    
    public void insert(Post post) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO posts(author, title, content)"
                + " VALUES(?, ?, ?)"
        );
        stmt.setString(1, post.getAuthor());
        stmt.setString(2, post.getTitle());
        stmt.setString(3, post.getContent());

        stmt.execute();
        this.disconnect();
    }


    public Post get(long id) throws SQLException {
        LOG.info("get(id)");
        Post post = new Post();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM posts"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
            post.setId(rs.getLong("id"));
            post.setAuthor(rs.getString("author"));
            post.setTitle(rs.getString("title"));
            post.setContent(rs.getString("content"));
            
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return post;
    }
}
