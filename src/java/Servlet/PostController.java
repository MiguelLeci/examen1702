/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Post;
import persistence.PostDAO;

/**
 *
 * @author usuario
 */
public class PostController extends BaseController {

    private static final Logger LOG = Logger.getLogger(PostController.class.getName());
    private PostDAO postDAO;
    private static final int ELEMENTS_PER_PAGE = 10;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void index(String page)    {
        postDAO = new PostDAO();
        int pageNum = 1;
        ArrayList<model.Post> posts = null;
        
        try {
            pageNum = Integer.parseInt(page);
        } catch(Exception ex) {
            LOG.log(Level.WARNING, "Error en el parámetro 'page'!!!", ex);
        }
        
        posts = postDAO.getPage(pageNum, ELEMENTS_PER_PAGE);
        
        request.setAttribute("posts", posts);
        dispatch("/WEB-INF/view/post/index.jsp");
    }


    public void view(String id) throws SQLException {
        postDAO = new PostDAO();
        Post post = new Post();
        long idNum;
        idNum = Integer.parseInt(id);
        post = postDAO.get( idNum );
        request.setAttribute("post", post);
        dispatch("/WEB-INF/view/post/view.jsp");
    }
    public void create() {
        dispatch("/WEB-INF/view/post/create.jsp");
    }

    public void store() throws IOException {

        //objeto persistencia
        postDAO = new PostDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Post post = loadFromRequest();
        
        synchronized (postDAO) {
            try {
                postDAO.insert(post);
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
            }
        }
        redirect(contextPath + "/post/index");
    }
    
    private Post loadFromRequest()
    {
        Post post = new Post();
        LOG.info("Crear modelo");
        post.setId(toId(request.getParameter("id")));
        post.setAuthor(request.getParameter("author"));
        post.setTitle(request.getParameter("title"));
        post.setContent(request.getParameter("content"));
        LOG.info("Datos cargados");
        return post;
    }
}
